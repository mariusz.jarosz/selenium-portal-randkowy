#!/usr/bin/python

from selenium import webdriver
from selenium.common.exceptions import MoveTargetOutOfBoundsException, NoSuchElementException
from selenium.webdriver.chrome.options import Options

from bs4 import BeautifulSoup

from pathlib import Path
import os

import csv

import time

import requests



import sys


folder_glowny = os.getcwd()

def zapisywanie(nr_folderu_z_nickami_i_niezapisanymi):
    folder_z_nickami = Path(f"{folder_glowny}\listy_nickow_{nr_folderu_z_nickami_i_niezapisanymi}\\")

    naglowek = ['nick', 'wiek', 'plec', 'miejscowosc', 'wojewodztwo', 'wyglad', 'formalne', 'dzieci', 'wartosci', 'znak_zodiaku', 'alkohol', 'papierosy', 'jezyki', 'o_mnie', 'osobowosc', 'szukam', 'zainteresowania', 'moj_wymarzony_partner']

    options = Options()
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")

    userAgent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.56 Safari/537.36"
    options.add_argument(f'user-agent={userAgent}')


    driver = webdriver.Chrome(options=options)
    driver.get("https://login.sympatia.onet.pl")
    driver.implicitly_wait(6)

    driver.find_element_by_class_name('cmp-intro_acceptAll ').click()
    driver.implicitly_wait(4)

    # LOGIN I HASŁO
    driver.find_element_by_name('email').send_keys('LOGIN')
    driver.find_element_by_name('password').send_keys('HASŁO')
    driver.find_element_by_class_name('default').click()
    driver.implicitly_wait(4)
    driver.find_element_by_class_name('ui-icon-closethick').click()
    driver.implicitly_wait(4)

    soup = BeautifulSoup(driver.page_source, 'lxml')
 

    for plik in os.listdir(folder_z_nickami):
        if plik.endswith(".txt"):
            sciezka_do_plikow_txt = f"{folder_z_nickami}\\{plik}"
            print(sciezka_do_plikow_txt)

        with open(f'{sciezka_do_plikow_txt}', 'r') as f:
            nicki = f.readlines().copy()

        plec = "kobieta" if plik.split("_")[1] == "k" else "mężczyzna"

        nazwa_sciezki_folderu = plik.split('.')[0]


        sciezka_do_zdjec = os.path.join(f'{folder_glowny}//zdjecia', nazwa_sciezki_folderu)
        if os.path.exists(sciezka_do_zdjec):
            pass
        else:
            os.mkdir(sciezka_do_zdjec)
        nazwa_pliku_csv = f"{nazwa_sciezki_folderu}.csv"
        plik_csv = Path(f'{folder_glowny}\csv\{nazwa_pliku_csv}')

        if os.path.exists(plik_csv):
            pass
        else:
            with open(plik_csv, 'w', encoding='UTF8', newline='') as f:
                csv.writer(f).writerow(naglowek)

        licznik = 1
        for n in range(len(nicki)):
            print('-------------------------')
            print(nicki[n])

            if licznik%25 == 0 or licznik%48 == 0:
                print('przerwa')
                time.sleep(2)
            print('-------------------------')
            print(f'nick nr {licznik} z {len(nicki)}')
            print('-------------------------')
            licznik += 1

            slownik_danych = {'nick':'','wiek':'','plec':plec, 'miejscowosc':'', 'wojewodztwo':'', 'wyglad':'', 'formalne':'', 'dzieci':'', 'wartosci':'', 'znak_zodiaku':'',
                'alkohol':'', 'papierosy':'', 'jezyki':'', 'o_mnie':'', 'osobowosc':'', 'szukam':'', 'zainteresowania':'', 'moj_wymarzony_partner':''}


            #  Zmienne do zapisania
            nick                    =   None
            wiek                    =   None   
            miejscowosc             =   None
            wojewodztwo             =   None
            wyglad                  =   None
            formalne                =   None
            dzieci                  =   None
            wartosci                =   None
            znak_zodiaku            =   None
            alkohol                 =   None
            papierosy               =   None
            jezyki                  =   None
            o_mnie                  =   None
            osobowosc               =   None
            szukam                  =   None
            zainteresowania         =   None
            moj_wymarzony_partner   =   None

            
            driver.get(f"https://sympatia.onet.pl/{nicki[n]}")
            time.sleep(1)
            driver.implicitly_wait(1)
            soup = BeautifulSoup(driver.page_source, 'lxml')
            if soup.find('div', class_="no-entry-message"):
                print(f'niezapisano {nicki[n]}')
                folder_niezapisane = Path(f"{folder_glowny}\\niezapisane\\")
                with open(f'{folder_niezapisane}\\lista_{plec}_niezapisane_{nr_folderu_z_nickami_i_niezapisanymi}.txt', 'a') as f:
                    f.write(f'{nicki[n]}\n')
                pass
            else:
                time.sleep(1)
                if soup.find('div', class_="region"):

                    region      =   soup.find('div', class_="region").get_text().strip().split(',')
                    if len(region) > 1:
                        miejscowosc =   region[0]
                        wojewodztwo =   region[1].strip()
                    else:
                        miejscowosc = region
                
                    # dane        =   soup.find('div', class_="nick").get_text().strip()
                    dane        =   soup.find('h1', class_="nick").text.strip().split(',')
                    

                    if len(dane) > 1:
                        nick        =   dane[0]
                        wiek        =   dane[1].strip()
                    else:
                        nick = dane
                    
                # Pobieranie zdjecia
                    zdjecie_zrodlo = soup.find("img", {"id": "userPhoto"})['src']
                    response = requests.get(zdjecie_zrodlo)
                    if response.status_code == 200:
                        with open(f'{sciezka_do_zdjec}\\{nick}.jpg', 'wb') as zdjecie:
                            zdjecie.write(response.content)

                    #  pozostałe elementy

                    elementy    =   soup.find_all("div", {"class": "infoGroup"})

                    for e in elementy:
                        etykieta    =   e.find("div", {"class": "label"}).text

                        wartosc     =   e.find("div", {"class": "value"}).text
                        wartosc = ' '.join(wartosc.split())
                        if etykieta == "Wygląd":
                            wyglad = wartosc
                        
                        if etykieta == "Formalne":
                            formalne = wartosc
                        
                        if etykieta == "Dzieci":
                            dzieci = wartosc
                        
                        if etykieta == "Wartości":
                            wartosci = wartosc
                        
                        if etykieta == "Znak zodiaku":
                            znak_zodiaku = wartosc
                        
                        if etykieta == "Alkohol":
                            alkohol = wartosc
                        
                        if etykieta == "Papierosy":
                            papierosy = wartosc
                        
                        if etykieta == "Języki":
                            jezyki = wartosc


                    elementy    =   soup.find_all("div", {"class": "prf_userNew_aboutMe"})
                    for e in elementy:

                        etykieta = e.find("h2").text
                        
                        if etykieta == "O mnie":
                            if e.find("div", {"class": "content"}):     
                                wartosc = e.find("div", {"class": "content"}).text
                                wartosc = ' '.join(wartosc.split())
                                o_mnie = wartosc
                            else:
                                o_mnie = None
                        if etykieta == "Osobowość":
                            if e.find("div", {"class": "content"}):
                                wartosc =e.find("div", {"class": "content"}).text
                                wartosc = ' '.join(wartosc.split())
                                osobowosc = wartosc
                            else:
                                osobowosc = None
                        if etykieta == "Szukam":
                            if e.find("div", {"class": "content"}):
                                wartosc =e.find("div", {"class": "content"}).text
                                wartosc = ' '.join(wartosc.split())
                                szukam = wartosc
                            else:
                                szukam = None
                        if etykieta == "Mój wymarzony partner":
                            if e.find("div", {"class": "content"}):
                                wartosc =e.find("div", {"class": "content"}).text
                                wartosc = ' '.join(wartosc.split())
                                moj_wymarzony_partner = wartosc
                            else:
                                moj_wymarzony_partner = None
                    zainteresowania_web_el = soup.find_all("span", {"class": "interestItems"})
                    if zainteresowania_web_el:
                        zainteresowania = [z.text for z in zainteresowania_web_el]
                        zainteresowania = ','.join([str(elem) for elem in zainteresowania])
                    else:
                        pass              
                
                    slownik_danych['nick']  =   nick                 
                    slownik_danych['wiek']  =   wiek                    
                    slownik_danych['miejscowosc']   =   miejscowosc          
                    slownik_danych['wojewodztwo']   =   wojewodztwo          
                    slownik_danych['wyglad']    =   wyglad               
                    slownik_danych['formalne']  =   formalne             
                    slownik_danych['dzieci']    =   dzieci               
                    slownik_danych['wartosci']  =   wartosci             
                    slownik_danych['znak_zodiaku']  =   znak_zodiaku         
                    slownik_danych['alkohol']   =   alkohol              
                    slownik_danych['papierosy'] =   papierosy            
                    slownik_danych['jezyki']    =   jezyki               
                    slownik_danych['o_mnie']    =   o_mnie               
                    slownik_danych['osobowosc'] =   osobowosc            
                    slownik_danych['szukam']    =   szukam               
                    slownik_danych['zainteresowania']   =   zainteresowania      
                    slownik_danych['moj_wymarzony_partner'] =   moj_wymarzony_partner

                    with open(plik_csv, 'a', encoding='UTF8', newline='') as f:
                        pisz = csv.DictWriter(f, fieldnames=naglowek)
                        pisz.writerow(slownik_danych)
                else:
                    print(f'niezapisano {nicki[n]}')
                    folder_niezapisane = Path(f"{folder_glowny}\\niezapisane\\")
                    with open(f'{folder_niezapisane}\\lista_{plec}_niezapisane_{nr_folderu_z_nickami_i_niezapisanymi}.txt', 'a') as f:
                        f.write(f'{nicki[n]}\n')
                    pass

nr_folderu_z_nickami_i_niezapisanymi = sys.argv[1]

zapisywanie(nr_folderu_z_nickami_i_niezapisanymi= nr_folderu_z_nickami_i_niezapisanymi)
                


                        
                





                