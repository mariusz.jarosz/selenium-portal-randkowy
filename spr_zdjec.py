from pathlib import Path
import os
import shutil


import cv2
import numpy as np



folder_glowny = os.getcwd()
sciezka_folder_zdjecia = Path(f"{folder_glowny}\\zdjecia")

oryginal_do_porownania = cv2.imread(f"{sciezka_folder_zdjecia}\\oryginal.jpg")

for zdjecie in os.listdir(f'{sciezka_folder_zdjecia}\\test'):
    if zdjecie.endswith(".jpg"):
        sciezka_do_zdjecia_do_sprawdzenia = f'{sciezka_folder_zdjecia}\\test\\{zdjecie}'
        do_sprawdzenia = cv2.imread(sciezka_do_zdjecia_do_sprawdzenia)

        # sprawdzenie, czy 2 zdjecia są identyczne
        if oryginal_do_porownania.shape == do_sprawdzenia.shape:
            # print(f'oryginal.jpg ma taki sam rozmiar jak zdjecie {zdjecie}')
            roznica = cv2.subtract(oryginal_do_porownania, do_sprawdzenia)
            b, g, r = cv2.split(roznica)
            if cv2.countNonZero(b) == 0 and cv2.countNonZero(g) == 0 and cv2.countNonZero(r) == 0:
                # print(f'oryginal.jpg jest identyczny jak zdjecie {zdjecie}')
                
                sciezka_do_przeniesienia = os.path.join(f'{sciezka_folder_zdjecia}//test//', 'bez_zdjecia')
                if os.path.exists(sciezka_do_przeniesienia):
                    # print('istnieje')
                    pass
                else:
                    os.mkdir(sciezka_do_przeniesienia)
                shutil.move(sciezka_do_zdjecia_do_sprawdzenia, os.path.join(sciezka_do_przeniesienia), zdjecie)
                print(f'przeniesiono zdjęcie {zdjecie}')
                

